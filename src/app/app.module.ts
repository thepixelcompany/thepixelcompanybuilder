import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';


import {AppComponent} from './app.component';

import {
  LogoNavBarComponent,
  HeaderComponent,
  LeftButtonsComponentComponent,
  LogoComponentComponent,
  RightButtonsComponentComponent,
  AppStoreComponent,
  SideBarNavComponent,
  ToolsSideBarComponent,
  PageDrawerComponent,
  MediaDrawerComponent,
  BottomToolbarComponent,
  NewChatPopUpComponent,
  ViewingOptionsComponent,
  ExportAndChatComponent,
  PublishingAndOnlineIndicatorComponent, ModalControllerComponent
} from './components';

import {CreatorSpaceComponent, ProjectSpaceOpeningComponent} from './views';

import {CreatorComponent, DaddyContainerComponent} from './containers';
import {
  PageModalComponent,
  ExportModalComponent,
  ChatModalComponent,
  InteractionsModalComponent,
  DraggableModalComponent,
  GlobalStylesComponent, ComingSoonModalComponent, PublishingMenuComponent
} from './modals';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const APP_CONTAINERS = [
  DaddyContainerComponent,
  CreatorComponent
];

const APP_VIEWS = [
  ProjectSpaceOpeningComponent,
  CreatorSpaceComponent
];

const APP_COMPONENT = [
  LogoNavBarComponent,
  HeaderComponent,
  LogoComponentComponent,
  LeftButtonsComponentComponent,
  RightButtonsComponentComponent,
  AppStoreComponent,
  SideBarNavComponent,
  ToolsSideBarComponent,
  PageDrawerComponent,
  MediaDrawerComponent,
  BottomToolbarComponent,
  NewChatPopUpComponent,
  ViewingOptionsComponent,
  ExportAndChatComponent,
  PublishingAndOnlineIndicatorComponent,
  ModalControllerComponent
];

const APP_MODALS = [
  PageModalComponent,
  ExportModalComponent,
  ChatModalComponent,
  InteractionsModalComponent,
  DraggableModalComponent,
  GlobalStylesComponent,
  ComingSoonModalComponent,
  PublishingMenuComponent
];

import {MaterialModuleModule, RouterRouting} from './modules';

const APP_MODULES = [
  MaterialModuleModule,
  RouterRouting
];

@NgModule({
  declarations: [
    AppComponent,
    ...APP_COMPONENT,
    ...APP_CONTAINERS,
    ...APP_VIEWS,
    ...APP_MODALS
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    ...APP_MODULES
  ],
  providers: [{provide: APP_BASE_HREF, useValue: '/'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
