import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportAndChatComponent } from './export-and-chat.component';

describe('ExportAndChatComponent', () => {
  let component: ExportAndChatComponent;
  let fixture: ComponentFixture<ExportAndChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportAndChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportAndChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
