import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftButtonsComponentComponent } from './left-buttons-component.component';

describe('LeftButtonsComponentComponent', () => {
  let component: LeftButtonsComponentComponent;
  let fixture: ComponentFixture<LeftButtonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftButtonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftButtonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
