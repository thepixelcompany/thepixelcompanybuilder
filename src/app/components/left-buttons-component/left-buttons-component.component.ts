import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-left-buttons-component',
  templateUrl: './left-buttons-component.component.html',
  styleUrls: ['./left-buttons-component.component.css']
})
export class LeftButtonsComponentComponent implements OnInit {

  @Input() modals: any[];

  constructor() { }

  ngOnInit() {
  }

  showModal(index) {
    this.modals[index].show();
  }

}
