import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaDrawerComponent } from './media-drawer.component';

describe('MediaDrawerComponent', () => {
  let component: MediaDrawerComponent;
  let fixture: ComponentFixture<MediaDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
