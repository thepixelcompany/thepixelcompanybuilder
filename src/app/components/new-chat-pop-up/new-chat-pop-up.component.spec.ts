import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewChatPopUpComponent } from './new-chat-pop-up.component';

describe('NewChatPopUpComponent', () => {
  let component: NewChatPopUpComponent;
  let fixture: ComponentFixture<NewChatPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewChatPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewChatPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
