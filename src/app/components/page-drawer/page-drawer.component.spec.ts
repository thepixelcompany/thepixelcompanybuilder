import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDrawerComponent } from './page-drawer.component';

describe('PageDrawerComponent', () => {
  let component: PageDrawerComponent;
  let fixture: ComponentFixture<PageDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
