import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishingAndOnlineIndicatorComponent } from './publishing-and-online-indicator.component';

describe('PublishingAndOnlineIndicatorComponent', () => {
  let component: PublishingAndOnlineIndicatorComponent;
  let fixture: ComponentFixture<PublishingAndOnlineIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishingAndOnlineIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishingAndOnlineIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
