import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightButtonsComponentComponent } from './right-buttons-component.component';

describe('RightButtonsComponentComponent', () => {
  let component: RightButtonsComponentComponent;
  let fixture: ComponentFixture<RightButtonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightButtonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightButtonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
