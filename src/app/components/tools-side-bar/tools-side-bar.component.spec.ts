import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolsSideBarComponent } from './tools-side-bar.component';

describe('ToolsSideBarComponent', () => {
  let component: ToolsSideBarComponent;
  let fixture: ComponentFixture<ToolsSideBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolsSideBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolsSideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
