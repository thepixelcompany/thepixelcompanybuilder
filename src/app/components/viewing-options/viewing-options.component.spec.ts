import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewingOptionsComponent } from './viewing-options.component';

describe('ViewingOptionsComponent', () => {
  let component: ViewingOptionsComponent;
  let fixture: ComponentFixture<ViewingOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewingOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewingOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
