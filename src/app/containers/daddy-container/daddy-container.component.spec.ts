import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaddyContainerComponent } from './daddy-container.component';

describe('DaddyContainerComponent', () => {
  let component: DaddyContainerComponent;
  let fixture: ComponentFixture<DaddyContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaddyContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaddyContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
