import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractionsModalComponent } from './interactions-modal.component';

describe('InteractionsModalComponent', () => {
  let component: InteractionsModalComponent;
  let fixture: ComponentFixture<InteractionsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteractionsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteractionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
