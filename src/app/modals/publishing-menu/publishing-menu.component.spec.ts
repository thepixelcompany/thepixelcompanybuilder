import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishingMenuComponent } from './publishing-menu.component';

describe('PublishingMenuComponent', () => {
  let component: PublishingMenuComponent;
  let fixture: ComponentFixture<PublishingMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishingMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishingMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
