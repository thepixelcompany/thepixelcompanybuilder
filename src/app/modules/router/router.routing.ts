import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CreatorComponent, DaddyContainerComponent} from '../../containers';

import {CreatorSpaceComponent, ProjectSpaceOpeningComponent} from '../../views';

const routes: Routes = [
  {
    path: '',
    component: DaddyContainerComponent,
    children: [
      {
        path: '',
        component: ProjectSpaceOpeningComponent
      }
    ]
  },
  {
    path: 'creator',
    component: CreatorComponent,
    children: [
      {
        path: '',
        component: CreatorSpaceComponent
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RouterRouting {
}
