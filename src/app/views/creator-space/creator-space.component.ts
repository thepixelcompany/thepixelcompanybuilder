import { Component, OnInit } from '@angular/core';
declare var grapesjs: any;

@Component({
  selector: 'app-creator-space',
  templateUrl: './creator-space.component.html',
  styleUrls: ['./creator-space.component.css']
})
export class CreatorSpaceComponent implements OnInit {

  editor;

  constructor() {}

  ngOnInit() {
    this.editor = grapesjs.init({
      // Indicate where to init the editor. You can also pass an HTMLElement
      container: '#gjs',
      // Get the content for the canvas directly from the element
      // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
      fromElement: true,
      // Size of the editor
      height: '300px',
      width: 'auto',
      // Disable the storage manager for the moment
      storageManager: { type: null },
      // Avoid any default panel
      panels: { defaults: [] },
    });
  }

}
