import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectSpaceOpeningComponent } from './project-space-opening.component';

describe('ProjectSpaceOpeningComponent', () => {
  let component: ProjectSpaceOpeningComponent;
  let fixture: ComponentFixture<ProjectSpaceOpeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectSpaceOpeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectSpaceOpeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
